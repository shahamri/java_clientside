package com.example.demo.UI;

import com.example.demo.entity.Product;
import com.example.demo.entity.ShoppingCart;
import com.example.demo.repository.CartRepository;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AppController {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CartRepository cartRepository;

    @GetMapping("/")
    public String home(Model model) {

        List<Product> listProducts = productRepository.findAll();
        model.addAttribute("listProducts", listProducts);
        List<ShoppingCart> listCart = cartRepository.findAll();
        model.addAttribute("listCart", listCart);
        return "main";
    }

}
